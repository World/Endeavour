/* gtd-debug.h
 *
 * Copyright (C) 2018 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 * Copyright (C) 2023 Jamie Murphy <hello@itsjamie.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "config.h"

#include <glib.h>

/**
 * SECTION: gtd-debug
 * @short_description: Debugging macros
 * @title:Debugging
 * @stability:stable
 *
 * Macros used for tracing and debugging code. These
 * are only valid when Endeavour is compiled with tracing
 * support (pass `--enable-tracing` to the configure
 * script to do that).
 */

G_BEGIN_DECLS

#ifndef GTD_ENABLE_TRACE
# define GTD_ENABLE_TRACE ENABLE_TRACING
#endif
#if GTD_ENABLE_TRACE != 1
# undef GTD_ENABLE_TRACE
#endif

/**
 * GTD_LOG_LEVEL_TRACE: (skip)
 */
#ifndef GTD_LOG_LEVEL_TRACE
# define GTD_LOG_LEVEL_TRACE ((GLogLevelFlags)(1 << G_LOG_LEVEL_USER_SHIFT))
#endif

#ifdef GTD_ENABLE_TRACE

/**
 * GTD_TRACE_MSG:
 * @fmt: printf-like format of the message
 * @...: arguments for @fmt
 *
 * Prints a trace message.
 */
# define GTD_TRACE_MSG(fmt, ...)                                        \
   g_log(G_LOG_DOMAIN, GTD_LOG_LEVEL_TRACE, "  MSG: %s():%d: " fmt,     \
         G_STRFUNC, __LINE__, ##__VA_ARGS__)

/**
 * GTD_PROBE:
 *
 * Prints a probing message. Put this macro in the code when
 * you want to check the program reaches a certain section
 * of code.
 */
# define GTD_PROBE                                                      \
   g_log(G_LOG_DOMAIN, GTD_LOG_LEVEL_TRACE, "PROBE: %s():%d",           \
         G_STRFUNC, __LINE__)

/**
 * GTD_ENTRY:
 *
 * Prints an entry message. This shouldn't be used in
 * critical functions. Place this at the beginning of
 * the function, before any assertion.
 */
# define GTD_ENTRY                                                      \
   g_log(G_LOG_DOMAIN, GTD_LOG_LEVEL_TRACE, "ENTRY: %s():%d",           \
         G_STRFUNC, __LINE__)

/**
 * GTD_EXIT:
 *
 * Prints an exit message. This shouldn't be used in
 * critical functions. Place this at the end of
 * the function, after any relevant code. If the
 * function returns something, use GTD_RETURN()
 * instead.
 */
# define GTD_EXIT                                                       \
   G_STMT_START {                                                        \
      g_log(G_LOG_DOMAIN, GTD_LOG_LEVEL_TRACE, " EXIT: %s():%d",        \
            G_STRFUNC, __LINE__);                                        \
      return;                                                            \
   } G_STMT_END

/**
 * GTD_GOTO:
 * @_l: goto tag
 *
 * Logs a goto jump.
 */
# define GTD_GOTO(_l)                                                   \
   G_STMT_START {                                                        \
      g_log(G_LOG_DOMAIN, GTD_LOG_LEVEL_TRACE, " GOTO: %s():%d ("#_l")",\
            G_STRFUNC, __LINE__);                                        \
      goto _l;                                                           \
   } G_STMT_END

/**
 * GTD_RETURN:
 * @_r: the return value.
 *
 * Prints an exit message, and returns @_r. See #GTD_EXIT.
 */
# define GTD_RETURN(_r)                                                 \
   G_STMT_START {                                                        \
      g_log(G_LOG_DOMAIN, GTD_LOG_LEVEL_TRACE, " EXIT: %s():%d ",       \
            G_STRFUNC, __LINE__);                                        \
      return _r;                                                         \
   } G_STMT_END

#else

/**
 * GTD_PROBE:
 *
 * Prints a probing message.
 */
# define GTD_PROBE

/**
 * GTD_TRACE_MSG:
 * @fmt: printf-like format of the message
 * @...: arguments for @fmt
 *
 * Prints a trace message.
 */
# define GTD_TRACE_MSG(fmt, ...)

/**
 * GTD_ENTRY:
 *
 * Prints a probing message. This shouldn't be used in
 * critical functions. Place this at the beginning of
 * the function, before any assertion.
 */
# define GTD_ENTRY

/**
 * GTD_GOTO:
 * @_l: goto tag
 *
 * Logs a goto jump.
 */
# define GTD_GOTO(_l)   goto _l

/**
 * GTD_EXIT:
 *
 * Prints an exit message. This shouldn't be used in
 * critical functions. Place this at the end of
 * the function, after any relevant code. If the
 * function returns something, use GTD_RETURN()
 * instead.
 */
# define GTD_EXIT       return

/**
 * GTD_RETURN:
 * @_r: the return value.
 *
 * Prints an exit message, and returns @_r. See #GTD_EXIT.
 */
# define GTD_RETURN(_r) return _r
#endif

G_END_DECLS
