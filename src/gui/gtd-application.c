/* gtd-application.c
 *
 * Copyright (C) 2015-2020 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 * Copyright (C) 2022-2023 Jamie Murphy <hello@itsjamie.dev>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "GtdApplication"

#include "config.h"

#include "gtd-application.h"
#include "gtd-debug.h"
#include "gtd-log.h"
#include "gtd-manager.h"
#include "gtd-vcs.h"
#include "gtd-window.h"

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <glib/gi18n.h>

struct _GtdApplication
{
  AdwApplication         application;

  GtkWindow             *window;
  GtkWidget             *initial_setup;
};

static void           gtd_application_activate_action             (GSimpleAction        *simple,
                                                                   GVariant             *parameter,
                                                                   gpointer              user_data);

static void           gtd_application_show_about                  (GSimpleAction        *simple,
                                                                   GVariant             *parameter,
                                                                   gpointer              user_data);

static void           gtd_application_quit                        (GSimpleAction        *simple,
                                                                   GVariant             *parameter,
                                                                   gpointer              user_data);

static void           gtd_application_show_help                   (GSimpleAction        *simple,
                                                                   GVariant             *parameter,
                                                                   gpointer              user_data);

G_DEFINE_TYPE (GtdApplication, gtd_application, ADW_TYPE_APPLICATION)

static GOptionEntry cmd_options[] = {
  { "quit", 'q', 0, G_OPTION_ARG_NONE, NULL, N_("Quit Endeavour"), NULL },
  { "debug", 'd', 0, G_OPTION_ARG_NONE, NULL, N_("Enable debug messages"), NULL },
  { "version", 'v', 0, G_OPTION_ARG_NONE, NULL, N_("Print version information and exit"), NULL },
  { NULL }
};

static const GActionEntry gtd_application_entries[] = {
  { "activate", gtd_application_activate_action },
  { "about",  gtd_application_show_about },
  { "quit",   gtd_application_quit },
  { "help",   gtd_application_show_help }
};

static void
gtd_application_activate_action (GSimpleAction *simple,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
  GtdApplication *self = GTD_APPLICATION (user_data);

  gtk_window_present (GTK_WINDOW (self->window));
}

static void
gtd_application_show_about (GSimpleAction *simple,
                            GVariant      *parameter,
                            gpointer       user_data)
{
  GtdApplication *self;

  static const gchar *developers[] = {
    "Emmanuele Bassi <ebassi@gnome.org>",
    "Georges Basile Stavracas Neto <georges.stavracas@gmail.com>",
    "Isaque Galdino <igaldino@gmail.com>",
    "Patrick Griffis <tingping@tingping.se>",
    "Jamie Murphy <hello@itsjamie.dev>",
    "Saiful B. Khan <saifulbkhan@gmail.com>",
    NULL
  };

  static const gchar *designers[] = {
    "Allan Day <allanpday@gmail.com>",
    "Jakub Steiner <jimmac@gmail.com>",
    "Tobias Bernard <tbernard@gnome.org>",
    NULL
  };

  self = GTD_APPLICATION (user_data);

  adw_show_about_window (GTK_WINDOW (self->window),
                         "application-name", g_strjoin (NULL, _("Endeavour"), PACKAGE_NAME_SUFFIX, NULL),
                         "application-icon", APPLICATION_ID,
                         "version", GTD_VCS_TAG,
                         "copyright", _("Copyright \xC2\xA9 2015–2022 The Endeavour authors"),
                         "issue-url", "https://gitlab.gnome.org/World/Endeavour/-/issues",
                         "support-url", "https://matrix.to/#/#endeavour:gnome.org",
                         "website", "https://gitlab.gnome.org/World/Endeavour",
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "developers", developers,
                         "designers", designers,
                         "translator-credits", _("translator-credits"),
                         NULL);
}

static void
gtd_application_quit (GSimpleAction *simple,
                      GVariant      *parameter,
                      gpointer       user_data)
{
  GtdApplication *self = GTD_APPLICATION (user_data);

  gtk_window_destroy (self->window);
}

static void
gtd_application_show_help (GSimpleAction *simple,
                           GVariant      *parameter,
                           gpointer       user_data)
{
  GtdApplication *self = GTD_APPLICATION (user_data);
  GtkFileLauncher *file_launcher = gtk_file_launcher_new (g_file_new_for_uri ("help:endeavour"));

  gtk_file_launcher_launch (file_launcher, GTK_WINDOW (self->window), NULL, NULL, NULL);
}

GtdApplication *
gtd_application_new (void)
{
  return g_object_new (GTD_TYPE_APPLICATION,
                       "application-id", APPLICATION_ID,
                       "flags", G_APPLICATION_DEFAULT_FLAGS,
                       "resource-base-path", "/org/gnome/todo",
                       NULL);
}

static void
set_accel_for_action (GtdApplication *self,
                      const gchar    *detailed_action_name,
                      const gchar    *accel)
{
  const char *accels[] = { accel, NULL };

  gtk_application_set_accels_for_action (GTK_APPLICATION (self), detailed_action_name, accels);
}

static void
gtd_application_activate (GApplication *application)
{
  GTD_ENTRY;
  GtdApplication *self = GTD_APPLICATION (application);

  /* TODO: Add a new Initial Setup screen */
  gtk_window_present (GTK_WINDOW (self->window));

  GTD_EXIT;
}

static void
gtd_application_startup (GApplication *application)
{
  GtdApplication *self;

  GTD_ENTRY;

  self = GTD_APPLICATION (application);

  /* add actions */
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   gtd_application_entries,
                                   G_N_ELEMENTS (gtd_application_entries),
                                   self);

  set_accel_for_action (self, "app.quit", "<Control>q");
  set_accel_for_action (self, "app.help", "F1");

  G_APPLICATION_CLASS (gtd_application_parent_class)->startup (application);

  /* window */
  gtk_window_set_default_icon_name (APPLICATION_ID);
  self->window = GTK_WINDOW (gtd_window_new (self));

  GTD_EXIT;
}

static void
gtd_application_class_init (GtdApplicationClass *klass)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (klass);

  application_class->activate = gtd_application_activate;
  application_class->startup = gtd_application_startup;
}

static void
gtd_application_init (GtdApplication *self)
{
  g_application_add_main_option_entries (G_APPLICATION (self), cmd_options);

  gtd_log_init ();
}
